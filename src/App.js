import React, {Component, Suspense} from 'react';
import {NavLink, BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.scss';
import 'semantic-ui-css/semantic.css';
import routes from "./routes";
import {AnimatePresence, motion} from "framer-motion";
import history from './history'
import {Button, Container, NavbarToggler} from "reactstrap";

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import {Icon} from "semantic-ui-react";

const loading = () => <div className="animated fadeIn pt-3 text-center">
  <div className="sk-spinner sk-spinner-pulse"></div>
</div>;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebar: true,
    }
  }

  toggleSidebar=()=>{
    const {sidebar} = this.state
    this.setState({sidebar:!sidebar})
  }

  render() {
    const {sidebar} = this.state
    return (
      <BrowserRouter history={history}>
        <React.Suspense fallback={loading()}>
          <Button onClick={this.toggleSidebar} className="mr-2 position-absolute" style={{zIndex:5,left:"0px",top:"50%"}}><Icon name="bars" /></Button>
          <Switch>
            <Container fluid className="app">
              <AnimatePresence exitBeforeEnter initial={false}>
                {sidebar === true && (
                  <motion.div transition="linear" key="menu" className="aside"
                              initial={{width: 0, opacity: 0}}
                              animate={{width: 200, opacity: 1}}
                              exit={{width: 0, opacity: 0}}>
                    <ul className="side-nav">
                      <NavLink activeClassName="active" exact to="/" className="link"><i
                        className="cui-home icons font-2xl d-block "/>
                        Главная
                      </NavLink>
                      <NavLink activeClassName="active" exact to="/customers" className="link"><i
                        className="cui-people icons font-2xl d-block "/>
                        Заказчики
                      </NavLink>
                      <NavLink activeClassName="active" exact to="/providers" className="link"><i className="cui-briefcase icons font-2xl d-block "/>
                        Поставщики
                      </NavLink>
                      <NavLink activeClassName="active" exact to="/procurement-plans" className="link"><i
                        className="cui-pencil icons font-2xl d-block "/>
                        Планы закупок
                      </NavLink>
                      <NavLink activeClassName="active" exact to="/auctions" className="link"><i
                        className="cui-pie-chart icons font-2xl d-block "/>
                        Торги
                      </NavLink>
                      <NavLink activeClassName="active" exact to="/contracts" className="link"><i
                        className="cui-file icons font-2xl d-block "/>
                        Контракты
                      </NavLink>
                      <NavLink activeClassName="active" exact to="/bad" className="link"><i
                        className="cui-circle-x icons font-2xl d-block "/>
                        Недобросовестные
                      </NavLink>
                    </ul>
                  </motion.div>
                )}
              </AnimatePresence>
              <Suspense fallback={loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <route.component {...props} />
                        )}/>
                    ) : (null);
                  })}
                </Switch>
              </Suspense>
            </Container>
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
