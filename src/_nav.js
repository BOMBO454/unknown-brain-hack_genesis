export default {
  items: [
    {
      name: 'Заказчики',
      url: '/customers',
    },
    {
      name: 'Поставщики',
      url: '/providers',
    },
    {
      name: 'Планы закупок',
      url: '/procurement-plans',
    },
    {
      name: 'Торги',
      url: '/auctions',
    },
    {
      name: 'Контракты',
      url: '/contracts',
    },
  ]
};
