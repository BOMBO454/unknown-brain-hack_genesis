import api from "./index";

export default {
  getCustomers: () => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: '/Customer/',
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  }),

  getCustomer: (id) => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: `/Customer/${id}/`,
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}
