import api from "@api/index";

export default {
  getEmployees: () => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: '/employees',
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  }),
  getEmployee: (id) => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: `/employees/${id}`,
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}
