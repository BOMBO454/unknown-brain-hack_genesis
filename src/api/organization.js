import api from "@api/index";

export default {
  getOrganizations: () => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: '/organizations',
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  }),
  getOrganization: (id) => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: `/organizations/${id}`,
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}
