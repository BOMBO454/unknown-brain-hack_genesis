import api from "@api/index";

export default {
  getPersons: () => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: '/persons',
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  }),
  getPerson: (id) => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: `/persons/${id}`,
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}
