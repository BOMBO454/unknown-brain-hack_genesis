import api from "@api/index";

export default {
  getSuppliers: () => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: '/suppliers',
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  }),
  getSupplier: (id) => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: `/suppliers/${id}`,
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}
