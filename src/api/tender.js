import api from "./index";

export default {
  getTenders: () => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: '/tender',
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  }),

  getTender: (id) => new Promise((resolve, reject) => {
    api({
      method: 'get',
      url: `/tender/${id}`,
    })
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}
