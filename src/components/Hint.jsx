import React from "react"
import {Popup} from "semantic-ui-react";

export default function Hint(
  {children, content}
) {
  return (
    <Popup trigger={children}>{content}</Popup>
  )
}
