import React from "react"
import {motion} from "framer-motion";
import {Spinner} from "reactstrap";

export default function Loading(
  {load, children}
) {
  return (
    <>
      {load ? (
        <Spinner style={{width: '3rem', height: '3rem'}}/>
      ) : (
        <motion.div
          key="rightSideBar"
          transition={{ease: "easeInOut", duration: 0.2}}
          initial={{opacity: 0, scale: 0.8}}
          animate={{opacity: 1, scale: 1}}
          exit={{opacity: 0, scale: 0.8}}>
          {children}
        </motion.div>
      )}
    </>
  )
}
