import React, {Component} from 'react';
import {Table} from "reactstrap";
import PropTypes from 'prop-types'

class BlockTable extends Component {
  constructor(prop) {
    super(prop);
    this.state = {}
  }

  componentDidMount() {

  }

  render() {
    const {header, data} = this.props
    console.log('data', data)
    return (
      <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
        <thead className="thead-light">
        <tr>
          {header && Object.keys(header).map(key => {
            const value = header[key]
            return (
              <th key={key} className="text-center">{value}</th>
            )
          })}
        </tr>
        </thead>
        <tbody>
        {data?.[0] && data.map((row,i)=> {
          return(
            <tr key={i}>
              {header && Object.keys(header).map(key => {
                const value = row[key]
                return (
                  <td key={key} className="text-center">
                    {value}
                  </td>
                )
              })}
            </tr>
          )
        })}
        </tbody>
      </Table>
    );
  }
}

BlockTable.propTypes = {
  header: PropTypes.object,
  data: PropTypes.object,
}

export default BlockTable;
