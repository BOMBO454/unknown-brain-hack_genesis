import React from 'react';
import {Button, Icon, Label} from "semantic-ui-react";

export default function ButtonSave({title = "Сохранить собранные данные", onClick}) {
  return (
    <Button onClick={onClick} as='div' labelPosition='left'>
      <Label color="blue" basic>
        {title}
      </Label>
      <Button color="blue" icon>
        <Icon name='fork'/>
      </Button>
    </Button>
  );
}
