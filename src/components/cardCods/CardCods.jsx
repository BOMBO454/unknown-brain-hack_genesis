import React, {Component} from 'react';
import {Card, CardBody, CardTitle, Table} from "reactstrap";
import faker from "faker"
faker.locale = "ru"

export default function CardCods(
  {organization}
) {
  const _organization = organization ? organization : {}
  const {full_name, id, inn, kpp, legalAddress, ogrn, okato, okfs, okopf, okpo, oktmo, postalAddress, registration_date, short_name, timeZone, type_org} = _organization
  return (
      <Card className="border-0 shadow-sm">
        <CardBody>
          <CardTitle className="text-center font-2xl">
            <strong>КОДЫ Классификаци</strong>
          </CardTitle>
          <Table borderless>
            <tbody>
            <tr>
              <td>ОКПО</td>
              <td>{okpo?.name}</td>
            </tr>
            <tr>
              <td>ОКАТО</td>
              <td>{okato?.name}</td>
            </tr>
            <tr>
              <td>ОКТМО</td>
              <td>{oktmo?.name}</td>
            </tr>
            <tr>
              <td>ОКФС</td>
              <td>{okfs?.name}</td>
            </tr>
            <tr>
              <td>ОКОПФ</td>
              <td>{okopf?.name}</td>
            </tr>
            <tr>
              <td>ЭКВЭД2</td>
              <td></td>
            </tr>
            <tr>
              <td>ОКФС</td>
              <td>{okfs?.name}</td>
            </tr>
            </tbody>
          </Table>
        </CardBody>
      </Card>
  )
}
