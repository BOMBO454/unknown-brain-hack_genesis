import React from 'react';
import {Card, CardBody, CardTitle} from "reactstrap";

export default function CardIUKL(
  {customer}
) {
  const _customer = customer ? customer : {}
  const {ikul, ikul_assigmentDate} = _customer
  return (
    <Card className="border-0 shadow-sm">
      <CardBody>
        <CardTitle className="font-2xl">
          ИКЮЛ: {ikul?.name}
        </CardTitle>
        <div className="d-flex justify-content-between flex-wrap">
          {/*<span>{type_org}</span>*/}
          <span>Присвоен:{ikul_assigmentDate}</span>
        </div>
      </CardBody>
    </Card>
  )
}
