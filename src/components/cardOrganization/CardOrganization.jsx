import React from 'react';
import {Card, CardBody, CardTitle, ListGroup, ListGroupItem} from "reactstrap";
import {Icon, Label} from "semantic-ui-react";

export default function CardOrganization(
  {organization}
) {
  const _organization = organization ? organization : {}
  const {full_name, id, inn, kpp, legalAddress, ogrn, okato, okfs, okopf, okpo, oktmo, postalAddress, registration_date, short_name, timeZone, type_org} = _organization
  return (
    <Card className="border-0 shadow-sm">
      <CardBody>
        <CardTitle className="text-center font-2xl">
          <strong>Организация</strong><br/>
          <strong>{full_name}</strong>
        </CardTitle>
        {short_name}
        <ListGroup flush>
          <ListGroupItem><Label>
            <Icon name='calendar'/> Дата регистрации
            <Label.Detail>{registration_date}</Label.Detail>
          </Label></ListGroupItem>
          <ListGroupItem>
            <strong>
              {type_org === "L" ? "Юридическое лицо" : "Физическое лицо"}
            </strong>
          </ListGroupItem>
          <ListGroupItem>Инн: <strong>{inn}</strong></ListGroupItem>
          <ListGroupItem>kpp: <strong>{kpp}</strong></ListGroupItem>
          <ListGroupItem>legalAddress: <strong>{legalAddress}</strong></ListGroupItem>
          <ListGroupItem>ogrn: <strong>{ogrn}</strong></ListGroupItem>
        </ListGroup>
      </CardBody>
    </Card>
  )
}

const a = {
  full_name: "Акционерное общество РЖД",
  id: 1,
  inn: "111111111111",
  kpp: "111111111",
  legalAddress: "1111111",
  ogrn: "1111111111111",
  okato: {id: 1, code: "123", name: "123", parent: "0"},
  okfs: {id: 1, code: "12", name: "123"},
  okopf: {id: 1, code: "123", name: "123", parent: "123"},
  okpo: {id: 1, code: "11", name: "11"},
  oktmo: {id: 1, code: "123", name: "1231", parent: "123"},
  postalAddress: "1111",
  registration_date: "2020-12-01T16:48:34Z",
  short_name: "АО РЖД",
  timeZone: {id: 1, offset: -4, name: "1231"},
  type_org: "L"
}
