import React, {Component} from 'react';
import {Card, CardBody, CardFooter, CardHeader, Progress} from "reactstrap"
import {Button} from "semantic-ui-react";
import {NavLink} from "react-router-dom";

class CardTender extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {} = this.state
    return (
      <Card className="border-0 shadow-sm">
        <CardHeader className="bg-white font-weight-bold font-lg">
          Поставка брусьев
        </CardHeader>
        <CardBody>
          <div className="d-flex justify-content-between align-items-start">
            <div>Стоимость: 6 000 000р</div>
            <div className="progressWrapper">
              <div>До 12.03.12</div>
              <Progress value={10}/>
            </div>
          </div>
          {/*мапить данные о пользователях*/}
          <div className="d-flex justify-content-between mt-3">
            <div className="w-75 font-weight-bold">ООО Деревянный парус</div>
            <div className="w-25">
              <Progress value={90} color="success">
                {90}%
              </Progress>
            </div>
          </div>
          <div className="d-flex justify-content-between mt-1">
            <div className="w-75 font-weight-bold" >ООО Деревянный парус</div>
            <div className="w-25">
              <Progress value={70} color="warn">
                {70}%
              </Progress>
            </div>
          </div>
        </CardBody>
        <CardFooter className="d-flex justify-content-around align-items-center bg-white">
          <NavLink to="/auction"><Button basic color='blue'> Другие участники </Button></NavLink>
          <Button basic color='blue'> Подробнее </Button>
        </CardFooter>
      </Card>
    );
  }
}

export default CardTender;
