import React from "react"
import {Card, CardBody, Progress} from "reactstrap";
import faker from "faker"

export default function CardWinner(
  {percent, name}
) {
  // const _organization = organization ? organization : {}
  // const {full_name, id, inn, kpp, legalAddress, ogrn, okato, okfs, okopf, okpo, oktmo, postalAddress, registration_date, short_name, timeZone, type_org} = _organization
  return (
    <Card className="border-0 shadow-sm">
      <CardBody>
        {name}
        <div className="d-flex justify-content-between mt-3">
          <div className="w-75 font-weight-bold">{faker.company.companyName()}</div>
          <div className="w-25">
            <Progress value={90} color="success">
              {90}%
            </Progress>
          </div>
        </div>
      </CardBody>
    </Card>
  )
}
