import React from "react"
import {Popup} from "semantic-ui-react";

export default function CellOrganization(
  {organization}
) {
  const _organization = organization ? organization : {}
  const {full_name, id, inn, kpp, legalAddress, ogrn, okato, okfs, okopf, okpo, oktmo, postalAddress, registration_date, short_name, timeZone, type_org} = _organization
  return (
    <a href="#">
        {full_name}
    </a>
  )
}
