import React from "react"
import {Popup} from "semantic-ui-react";
import {Progress} from "reactstrap";

export default function CellWinner(
  {percent,name}
) {
  // const _organization = organization ? organization : {}
  // const {full_name, id, inn, kpp, legalAddress, ogrn, okato, okfs, okopf, okpo, oktmo, postalAddress, registration_date, short_name, timeZone, type_org} = _organization
  return (
    <div>
      {name}
      <Progress value={percent}>{percent}%</Progress>
    </div>
  )
}
