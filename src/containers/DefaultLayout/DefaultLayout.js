import React, {Component, Suspense} from 'react';
import {NavLink, Redirect, Route, Switch} from 'react-router-dom';
import routes from '../../routes';

class DefaultLayout extends Component {
  loading = () => <div className="animated fadeIn pt-1 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>;

  signOut(e) {
    e.preventDefault()
    this.props.history.push('/login')
  }

  render() {
    return (
      <div className="app">
        <aside className="aside">
          <Nav>
            <NavLink to="/">Привет</NavLink>
          </Nav>
        </aside>
        <main className="main">
          <Suspense fallback={this.loading()}>
            <Switch>
              {routes.map((route, idx) => {
                return route.component ? (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (
                      <route.component {...props} />
                    )}/>
                ) : (null);
              })}
              <Redirect from="/" to="/dashboard"/>
            </Switch>
          </Suspense>
        </main>
      </div>
    );
  }
}

export default DefaultLayout;
