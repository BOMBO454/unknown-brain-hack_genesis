import React from 'react';

import Customers from './views/customers/Customers';
import Providers from './views/providers/Providers';
import ProcurementPlans from './views/procurementPlans/ProcurementPlans';
import Auctions from './views/auctions/Auctions';
import Contracts from './views/contracts/Contracts';
import Dashboard from './views/_Example/Dashboard';
import Home from "./views/Home";
import Auction from "./views/auctions/Auction";

const routes = [
  {path: '/', exact: true, name: 'Home', component: Home},
  {path: '/dashboard', exact: true, name: 'Dashboard', component: Dashboard},
  {path: '/customers', exact: true, name: 'Customers', component: Customers},
  {path: '/providers', exact: true, name: 'Providers', component: Providers},
  {path: '/procurement-plans', exact: true, name: 'ProcurementPlans', component: ProcurementPlans},
  {path: '/auctions', exact: true, name: 'Auctions', component: Auctions},
  {path: '/auction', exact: true, name: 'Auctions', component: Auction},
  {path: '/contracts', exact: true, name: 'Contracts', component: Contracts},
  {path: '/bad', exact: true, name: 'Contracts', component: Contracts},
];

export default routes;
