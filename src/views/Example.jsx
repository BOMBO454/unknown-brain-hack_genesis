import React, {useState} from 'react'
import {
  Button, Table, Card, CardBody, CardHeader, CardFooter, Nav, NavItem, NavLink,
  TabPane, TabContent, Row, Col, CardTitle, CardText
} from "reactstrap";
import classnames from 'classnames';

export default function Example() {
  const [isOpen, setIsOpen] = useState('');
  const toggle = (id) => {
    if (id === isOpen) {
      setIsOpen('');
    } else {
      setIsOpen(id);
    }
  }

  const [activeTab, setActiveTab] = useState('1');

  const toggleTab = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }

  let fields = [
    {key: 'id', label: 'Id'},
    {key: 'firstName', label: 'firstName'},
    {key: 'secondName', label: 'secondName'},
    {key: 'btn', label: 'Развернуть'}
  ];

  let items = [
    {id: '1', firstName: 'Игнатий', secondName: 'Гришин', btn: ''},
    {id: '2', firstName: 'Игнатий', secondName: 'Гришин', btn: <Button color="primary" onClick={() => toggle(2)}/>},
  ];

  return (
    <Table>
      <thead>
      <tr>
        {fields.map((item) => <th>{item.label}</th>)}
      </tr>
      </thead>
      <tbody>
      {items.map((item) => (
        <>
          <tr key={item.id}>
            {fields.map(field => {
              if (field.key === 'btn') {
                return (
                  <td>
                    <Button color="primary" onClick={() => toggle(item.id)}>
                      {isOpen !== item.id ? 'show more' : 'close'}
                    </Button>
                  </td>
                )
              } else {
                return (
                  <td>
                    {item[field.key]}
                  </td>
                )
              }
            })}
          </tr>
          {isOpen === item.id && (
            <tr>
              <td colSpan={12}>
                <Nav tabs>
                  <NavItem>
                    <NavLink
                      className={classnames({active: activeTab === '1'})}
                      onClick={() => {
                        toggleTab('1');
                      }}
                    >
                      Tab1
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({active: activeTab === '2'})}
                      onClick={() => {
                        toggleTab('2');
                      }}
                    >
                      More Tabs
                    </NavLink>
                  </NavItem>
                </Nav>
                <TabContent activeTab={activeTab}>
                  <TabPane tabId="1">
                    <Row>
                      <Col>
                        <Card>
                          <CardBody>
                            With supporting text below as a natural lead-in to additional content.
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                  </TabPane>
                  <TabPane tabId="2">
                    <Row>
                      <Col>
                        <Card>
                          <CardBody>
                            GhbdtnGhbdtnGhbdtnGhbdtnGhbdtnGhbdtnGhbdtnGhbdtnGhbdtn
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                  </TabPane>
                </TabContent>
              </td>
            </tr>
          )}
        </>
      ))}
      </tbody>
    </Table>
  )
}
