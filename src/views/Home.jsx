import React from 'react'
import CardOrganization from "../components/cardOrganization/CardOrganization";
import BlockTable from "../components/blockTable/BlockTable";
import {Button, ButtonGroup, Card, CardBody, CardHeader} from "reactstrap";
import {motion} from "framer-motion"
import CardIUKL from "../components/cardIUKL/CardIUKL";
import CardCods from "../components/cardCods/CardCods";
import ButtonSave from "../components/buttonSave/ButtonSave";
import customer from "../api/customer";
import CellOrganization from "../components/cellOrganization/CellOrganization";
import Loading from "../components/Loading";


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      activeTab: 1,
      customers: [],
      customersData: [],
      customersHeader: [],
      customer: undefined,
      load: true,
      loadExtra: true,
    }
  }

  componentDidMount() {
    this.getCustomers()
  }

  showDetail = (id) => {
    this.setState({loadExtra: 2})
    customer.getCustomer(id).then(({data}) => {
      console.log('data', data)
      this.setState({customer: data})
    }).finally(() => {
      this.setState({loadExtra: false})
    })
  }

  getCustomers = () => {
    customer.getCustomers().then(({data}) => {
      const customers = data?.results.map(customer => ({
        id: customer.id,
        contactPerson: `${customer?.contactPerson?.lastName} ${customer?.contactPerson?.firstName} ${customer?.contactPerson?.middleName}`,
        organization: <CellOrganization organization={customer?.organization}/>,
        url: (<a href={customer?.url} target="_blank">{customer?.url}</a>),
        more: (<Button onClick={() => this.showDetail(customer.id)}>Подробнее</Button>),
      }))
      this.setState({customersData: customers})
    }).finally(() => {
      this.setState({load: false})
    })
  }

  setActiveTab = id => {
    this.setState({activeTab: id})
  }

  toggle = tab => {
    const {activeTab} = this.state
    if (activeTab !== tab) this.setActiveTab(tab);
  }

  render() {
    const {visible, activeTab, customersData, customer, load, loadExtra} = this.state
    return (
      <>
        <Card className="mainCard">
          <CardHeader className="bg-white">
            <motion.h1 key="rightSideBar" transition={{ease: "easeInOut", duration: 0.3}} initial={{opacity: 0, x: 200}}
                       animate={{opacity: 1, x: 0}}
                       exit={{opacity: 0, x: -200}}
                       className="h1 bebas mt-2">
              Главная
            </motion.h1>
          </CardHeader>
          <CardBody>
            <Loading load={load}>

              <BlockTable header={{id: "id", contactPerson: "Имя", organization: "Организатор", url: "url", more: ""}}
                          data={customersData}/>
            </Loading>
          </CardBody>
        </Card>
        {(loadExtra === 2 || loadExtra === false) && (
          <div className="aside aside--right pl-2 d-flex flex-column align-items-center">
            <Loading load={loadExtra}>
              <>
                <ButtonGroup size="lg">
                  <Button outline={activeTab !== 1} color="primary" onClick={() => {
                    this.toggle(1)
                  }}>Общее</Button>
                  <Button outline={activeTab !== 2} color="primary" onClick={() => {
                    this.toggle(2)
                  }}>Сравнение</Button>
                </ButtonGroup>
                <div className="aside--right--content">
                  {activeTab === 1 && customer && (
                    <motion.div key="contracts" initial={{opacity: 0, scale: 0.8}} animate={{opacity: 1, scale: 1}}
                                exit={{opacity: 0, scale: 0.8}}>
                      <CardOrganization organization={customer?.organization}/>
                      <CardIUKL customer={customer}/>
                      <CardCods organization={customer?.organization}/>
                      <ButtonSave/>
                    </motion.div>
                  )}
                </div>
              </>
            </Loading>

          </div>
        )}
      </>
    )
  }
}

export default Home
