import React from 'react'
import faker from 'faker'
import CardOrganization from "../../components/cardOrganization/CardOrganization";
import BlockTable from "../../components/blockTable/BlockTable";
import {Button, ButtonGroup, Card, CardBody, CardHeader} from "reactstrap";
import {motion} from "framer-motion"
import CardIUKL from "../../components/cardIUKL/CardIUKL";
import CardCods from "../../components/cardCods/CardCods";
import ButtonSave from "../../components/buttonSave/ButtonSave";
import customer from "../../api/customer";
import CellOrganization from "../../components/cellOrganization/CellOrganization";
import Loading from "../../components/Loading";
import CardWinner from "../../components/cellWinner/CellWinner";
import {Bar} from "react-chartjs-2";
import {CustomTooltips} from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import CardTender from "../../components/cardTender/CardTender";

const bar = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'Пример графика',
      backgroundColor: 'rgba(51,153,255,0.2)',
      borderColor: 'rgba(51,153,255,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(51,153,255,0.4)',
      hoverBorderColor: 'rgba(51,153,255,1)',
      data: [65, 59, 80, 81, 56, 55, 40],
    },
  ],
}

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false
}

class Auctions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      activeTab: 1,
      customers: [],
      customersData: [],
      customersHeader: [],
      customer: undefined,
      load: true,
      loadExtra: true,
    }
  }

  componentDidMount() {
    this.getCustomers()
  }

  showDetail = (id) => {
    this.setState({loadExtra: 2, activeTab:2})
    customer.getCustomer(id).then(({data}) => {
      console.log('data', data)
      this.setState({customer: data})
    }).finally(() => {
      this.setState({loadExtra: false})
    })
  }

  getCustomers = () => {
    customer.getCustomers().then(({data}) => {
      const customers = data?.results.map(customer => ({
        id: customer.id,
        contactPerson: `${customer?.contactPerson?.lastName} ${customer?.contactPerson?.firstName} ${customer?.contactPerson?.middleName}`,
        organization: <CellOrganization organization={customer?.organization}/>,
        url: (<a href={customer?.url} target="_blank">{customer?.url}</a>),
        tender: faker.lorem.words(),
        price: Math.ceil(faker.finance.amount() * 100),
        winner: (<CardWinner name={faker.company.companyName()} percent={Math.ceil(Math.random() * 100)}/>),
        more: (<Button onClick={() => this.showDetail(customer.id)}>Подробнее</Button>),
      }))
      this.setState({customersData: customers})
    }).finally(() => {
      this.setState({load: false})
    })
  }

  setActiveTab = id => {
    this.setState({activeTab: id})
  }

  toggle = tab => {
    const {activeTab} = this.state
    if (activeTab !== tab) this.setActiveTab(tab);
  }

  render() {
    const {visible, activeTab, customersData, customer, load, loadExtra} = this.state
    return (
      <>
        <Card className="mainCard">
          <CardHeader className="bg-white">
            <motion.h1 key="rightSideBar" transition={{ease: "easeInOut", duration: 0.3}} initial={{opacity: 0, x: 200}}
                       animate={{opacity: 1, x: 0}}
                       exit={{opacity: 0, x: -200}}
                       className="h1 bebas mt-2">
              Контракты
            </motion.h1>
          </CardHeader>
          <CardBody>
            <Loading load={load}>

              <BlockTable header={{
                id: "id",
                organization: "Организатор",
                tender: "Предмет тендера",
                price: "Бюджет",
                winner: "Потенциальный победитель",
                more: ""
              }}
                          data={customersData}/>
            </Loading>
          </CardBody>
        </Card>
        <div className="aside aside--right pl-2 d-flex flex-column align-items-center">
          <ButtonGroup size="lg">
            <Button outline={activeTab !== 1} color="primary" onClick={() => {
              this.toggle(1)
            }}>Общее</Button>
            <Button outline={activeTab !== 2} color="primary" onClick={() => {
              this.toggle(2)
            }}>Подробно</Button>
          </ButtonGroup>
          <div className="aside--right--content">
            {activeTab === 1 && (
              <motion.div key="contracts" initial={{opacity: 0, scale: 0.8}} animate={{opacity: 1, scale: 1}}
                          exit={{opacity: 0, scale: 0.8}}>
                <Card className="border-0 shadow-sm">
                  <CardHeader className="bg-white">
                    Bar Chart
                    <div className="card-header-actions">
                      <a href="http://www.chartjs.org" className="card-header-action">
                        <small className="text-muted">docs</small>
                      </a>
                    </div>
                  </CardHeader>
                  <CardBody>
                    <div className="chart-wrapper">
                      <Bar data={bar} options={options} />
                    </div>
                  </CardBody>
                </Card>
              </motion.div>
            )}
            {activeTab === 2 && customer && (
              <motion.div key="contracts" initial={{opacity: 0, scale: 0.8}} animate={{opacity: 1, scale: 1}}
                          exit={{opacity: 0, scale: 0.8}}>
                <CardOrganization organization={customer?.organization}/>
                <CardTender />
                <CardTender />
                <CardTender />
              </motion.div>
            )}
          </div>
        </div>
      </>
    )
  }
}

export default Auctions
