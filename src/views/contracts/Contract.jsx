import React, {useState} from 'react';
import {Card, CardBody, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane} from "reactstrap";
import classnames from "classnames";

export default function Contract({id}){

  const [activeTab, setActiveTab] = useState('1');

  const toggleTab = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }

  return (
    <tr>
      <td colSpan={12}>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({active: activeTab === '1'})}
              onClick={() => {
                toggleTab('1');
              }}
            >
              Tab1
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({active: activeTab === '2'})}
              onClick={() => {
                toggleTab('2');
              }}
            >
              More Tabs
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col>
                <Card>
                  <CardBody>
                    With supporting text below as a natural lead-in to additional content.
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col>
                <Card>
                  <CardBody>
                    GhbdtnGhbdtnGhbdtnGhbdtnGhbdtnGhbdtnGhbdtnGhbdtnGhbdtn
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </td>
    </tr>
  )
}
