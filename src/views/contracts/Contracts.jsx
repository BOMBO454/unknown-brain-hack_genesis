import React from 'react'
import CardOrganization from "../../components/cardOrganization/CardOrganization";
import BlockTable from "../../components/blockTable/BlockTable";
import {Button, ButtonGroup, Card, CardBody, CardHeader} from "reactstrap";
import {motion} from "framer-motion"
import CardIUKL from "../../components/cardIUKL/CardIUKL";
import CardCods from "../../components/cardCods/CardCods";
import ButtonSave from "../../components/buttonSave/ButtonSave";
import customer from "../../api/customer";

class Contracts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      activeTab: 1
    }
  }

  componentDidMount() {
    customer.getCustomers().then(({data}) => {
      console.log('data', data)
      console.table(data)
    })
  }

  setActiveTab = id => {
    this.setState({activeTab: id})
  }

  toggle = tab => {
    const {activeTab} = this.state
    if (activeTab !== tab) this.setActiveTab(tab);
  }

  render() {
    const {visible, activeTab} = this.state
    return (
      <>
        <div className="flex-grow-1">
          <h1 className="h1 bebas">Контракты</h1>
          <Card className="mainCard h-100">
            <CardHeader className="bg-white"></CardHeader>
            <CardBody>
              <BlockTable header={{id: "id", name: "Имя", organisator: "Организатор", winner: "победитель"}}
                          data={[{
                            id: 1, name: "lol", organisator: <Button basic color='blue' onClick={() => {
                              this.setState({visible: true})
                            }}>Подробнее</Button>,
                            winner: <Button basic color='blue'>Подробнее</Button>,
                          }]}/>
            </CardBody>
          </Card>
        </div>
        <div className="aside aside--right pl-2 d-flex flex-column align-items-center">
          <ButtonGroup size="lg">
            <Button outline={activeTab !== 1} color="primary" onClick={() => {
              this.toggle(1)
            }}>Общее</Button>
            <Button outline={activeTab !== 2} color="primary" onClick={() => {
              this.toggle(2)
            }}>Сравнение</Button>
          </ButtonGroup>
          <div className="aside--right--content">
            {activeTab === 1 && (
              <motion.div key="contracts" initial={{opacity: 0, scale: 0.8}} animate={{opacity: 1, scale: 1}}
                          exit={{opacity: 0, scale: 0.8}}>
                <CardOrganization/>
                <CardIUKL/>
                <CardCods/>
                <ButtonSave/>
              </motion.div>
            )}
          </div>
        </div>
      </>
    )
  }
}

export default Contracts
