import {
  Breadcrumbs,
  Cards,
  Carousels,
  Collapses,
  Dropdowns,
  Jumbotrons,
  ListGroups,
  Navbars,
  Navs,
  Paginations,
  Popovers,
  ProgressBar,
  Switches,
  Tabs,
  Tooltips,
} from './_Example/Base';

import { BrandButtons, ButtonDropdowns, ButtonGroups, Buttons, LoadingButtons } from './_Example/Buttons';
import Charts from './_Example/Charts';
import Dashboard from './_Example/Dashboard';
import { CodeEditors, TextEditors } from './_Example/Editors';
import { AdvancedForms, BasicForms, ValidationForms } from './_Example/Forms';
import ReactGoogleMaps from './_Example/GoogleMaps';
import { CoreUIIcons, Flags, FontAwesome, SimpleLineIcons } from './_Example/Icons';
import { Alerts, Badges, Modals, Toastr } from './_Example/Notifications';
import { Login, Page404, Page500, Register } from './_Example/Pages';
import { Calendar, Spinners } from './_Example/Plugins';
import { DataTable, Tables } from './_Example/Tables';
import { Colors, Typography } from './_Example/Theme';
import { Compose, Inbox, Invoice, Message } from './_Example/Apps';
import Widgets from './_Example/Widgets';

export {
  Badges,
  Typography,
  Colors,
  CoreUIIcons,
  Page404,
  Page500,
  Register,
  Login,
  Modals,
  Alerts,
  Flags,
  SimpleLineIcons,
  FontAwesome,
  ButtonDropdowns,
  ButtonGroups,
  BrandButtons,
  Buttons,
  Tooltips,
  Tabs,
  Tables,
  Charts,
  Dashboard,
  Widgets,
  Jumbotrons,
  Switches,
  ProgressBar,
  Popovers,
  Navs,
  Navbars,
  ListGroups,
  Dropdowns,
  Collapses,
  Carousels,
  Cards,
  Breadcrumbs,
  Paginations,
  LoadingButtons,
  CodeEditors,
  TextEditors,
  AdvancedForms,
  BasicForms,
  ValidationForms,
  Toastr,
  Calendar,
  Spinners,
  DataTable,
  Inbox,
  Message,
  Compose,
  Invoice,
  ReactGoogleMaps
};

