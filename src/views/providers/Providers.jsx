import React from 'react';
import {Card, CardBody, CardHeader, Pagination, PaginationItem, PaginationLink} from "reactstrap";
import {Button, Tab} from "semantic-ui-react";

import tender from "../../api/tender";
import CardTender from "../../components/cardTender/CardTender";
import BlockTable from "../../components/blockTable/BlockTable";
import customer from "../../api/customer";
import CellOrganization from "../../components/cellOrganization/CellOrganization";
import {motion} from "framer-motion";
import Loading from "../../components/Loading";

class Providers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      activeTab: 1,
      customers: [],
      customersData: [],
      customersHeader: [],
      customer: undefined,
      load: true,
      loadExtra: true,
    }
  }

  componentDidMount() {
    this.getCustomers()
  }

  showDetail = (id) => {
    this.setState({loadExtra: 2})
    customer.getCustomer(id).then(({data}) => {
      console.log('data', data)
      this.setState({customer: data})
    }).finally(() => {
      this.setState({loadExtra: false})
    })
  }

  getCustomers = () => {
    customer.getCustomers().then(({data}) => {
      const customers = data?.results.map(customer => ({
        id: customer.id,
        contactPerson: `${customer?.contactPerson?.lastName} ${customer?.contactPerson?.firstName} ${customer?.contactPerson?.middleName}`,
        organization: <CellOrganization organization={customer?.organization}/>,
        url: (<a href={customer?.url} target="_blank">{customer?.url}</a>),
        more: (<Button onClick={() => this.showDetail(customer.id)}>Подробнее</Button>),
      }))
      this.setState({customersData: customers})
    }).finally(() => {
      this.setState({load: false})
    })
  }

  setActiveTab = id => {
    this.setState({activeTab: id})
  }

  toggle = tab => {
    const {activeTab} = this.state
    if (activeTab !== tab) this.setActiveTab(tab);
  }

  render() {
    const {visible, activeTab, customersData, customer, load, loadExtra} = this.state
    return (
      <>
        <Card className="mainCard">
          <CardHeader className="bg-white">
            <motion.h1 key="rightSideBar" transition={{ease: "easeInOut", duration: 0.3}} initial={{opacity: 0, x: 200}}
                       animate={{opacity: 1, x: 0}}
                       exit={{opacity: 0, x: -200}}
                       className="h1 bebas mt-2">
              Поставщики
            </motion.h1>
          </CardHeader>
          <CardBody>
            <Loading load={load}>

              <BlockTable header={{id: "id", contactPerson: "Имя", organization: "Организатор", url: "url", more: ""}}
                          data={customersData}/>
            </Loading>
          </CardBody>
        </Card>
        <div className="aside aside--right pl-2 d-flex flex-column align-items-center">
          <div className="aside--right--content w-100">
            <Tab
              menu={{secondary: true, pointing: true, borderless: true}}
              panes={[
                {
                  menuItem: "Общее", render: () => (
                    <Tab.Pane className="border-0 bg-transparent p-0">
                      <CardTender/>
                      <CardTender/>
                      <CardTender/>
                    </Tab.Pane>)
                },
                {
                  menuItem: "Сравнение",
                  render: () => <Tab.Pane className="border-0 bg-transparent p-0">Tab 1 Content</Tab.Pane>
                }
              ]}/>
            <Pagination className="d-flex justify-content-center align-items-center"
                        aria-label="Page navigation example">
              <PaginationItem disabled>
                <PaginationLink first href=""/>
              </PaginationItem>
              <PaginationItem disabled>
                <PaginationLink previous href=""/>
              </PaginationItem>
              <PaginationItem active>
                <PaginationLink href="">
                  1
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink href="">
                  2
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink href="">
                  3
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink href="">
                  4
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink href="">
                  5
                </PaginationLink>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink next href=""/>
              </PaginationItem>
              <PaginationItem>
                <PaginationLink last href=""/>
              </PaginationItem>
            </Pagination>
          </div>
        </div>
      </>
    )
  }
}

export default Providers;
